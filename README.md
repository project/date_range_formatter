Date Range Formatter
=====
## CONTENTS OF THIS FILE

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Maintainers

## INTRODUCTION

The module is an essential addition to the Drupal
ecosystem, specifically designed for enhancing the functionality and
presentation of date ranges. This module is particularly useful for websites
that display events, schedules, or any content with a start and end date.

## REQUIREMENTS

This module requires no modules outside of Drupal core.

## INSTALLATION

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## CONFIGURATION

1. Navigate to Content Type: Go to the 'Structure' section in your Drupal admin
   panel and select 'Content types.'
2. Choose the content type that contains the date range field you want to
   format. Click 'Manage fields.'
3. Find the date range field and click on the 'Edit' button next to it.
4. In the field settings, look for the 'Format' section. Here, you'll find new 
   options added by the module. Choose the format that best suits your needs.
5. Depending on the module's version and features, you might have various
   options like hiding the end date if it's the same as the start date, custom
   separators, and time formats.
6. After customizing the format, click the 'Save settings' button to apply your
   changes.
7. To test your new settings, create a new piece of content or edit an existing
   one that uses the date range field.
8. Fill in the date range fields and save the content. View the content to
   ensure that the date range is displayed according to your configured format.

## Maintainers

- Maxim Podorov - [maximpodorov](https://www.drupal.org/u/maximpodorov)
- SUDISHTH KUMAR - [sudishth](https://www.drupal.org/u/sudishth)
